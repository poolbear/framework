<?php

namespace Illuminate\Routing;

use BadMethodCallException;

abstract class Controller
{
    /**
     * 处理对控制器上缺失的方法的调用
     *
     * 调用的成员方法不存在
     *
     * @return void
     */
    public function __call($method, $parameters)
    {
        throw new BadMethodCallException(sprintf(
            'Method %s::%s 不存在.', static::class, $method
        ));
    }
}